package com.kozlov;

import com.kozlov.View.MyView;
import com.kozlov.model.LastTask.Client;
import com.kozlov.model.LastTask.Server;
import com.kozlov.model.ReaderTest.Writer;

public class Application {
    public static void main(String[] args) {
/*        MyView view = new MyView();
        view.show();*/
        Server  server = new Server();
        Client client = new Client();
        server.run();
        client.run();

    }
}
