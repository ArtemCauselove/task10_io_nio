package com.kozlov.Controller;

import com.kozlov.model.Comments.Comments;
import com.kozlov.model.Deserialize.DeSerialisation;
import com.kozlov.model.FolderContent;
import com.kozlov.model.ReaderTest.Reader;

public class Controller {
    Comments comment;
    DeSerialisation deser;
    Reader readTest;
    FolderContent folder;
    public Controller(){
        comment = new Comments();
        deser = new DeSerialisation();
        readTest = new Reader();
        folder = new FolderContent();
    }
    public void comments(String path){
        comment.printComments(path);
    }
    public void SerialDeserial(){
        deser.serialize();
        deser.deSerialize();
    }
    public void readTest(int size){
        readTest.bufferReading(size);
        //readTest.usualReading();
    }
    public void printFolder(String folderPath){
        folder.folderContent(folderPath);
    }

}
