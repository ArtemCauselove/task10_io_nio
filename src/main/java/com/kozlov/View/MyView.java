package com.kozlov.View;

import com.kozlov.Controller.Controller;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
public class MyView {
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    Controller controller = new Controller();
    private static Logger logger = LogManager.getLogger(MyView.class);

    public MyView() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        menu.put("1", "  1 - Serialisation and Deserialisation");
        menu.put("2", "  2 - Test Reading and Writing");
        menu.put("3", "  3 - Show comments");
        menu.put("4", "  4 - Show folder content");
        menu.put("Q", "  Q - exit");
        methodsMenu.put("1", this::serialisation);
        methodsMenu.put("2", this::readingWriting);
        methodsMenu.put("3", this::showComments);
        methodsMenu.put("4", this::showFolderContent);
    }

    private void serialisation(){
        controller.SerialDeserial();
    }
    private void readingWriting(){
        while(true) {
            logger.info("input buffer size");
            String size = input.nextLine();
            if(size.equals("end")){
                break;
            }
            controller.readTest(Integer.valueOf(size));
        }
    }
    private void showComments(){
        while(true) {
            logger.info("input path for file");
            String path = input.nextLine();
            if(path.equals("end")){
                break;
            }
            controller.comments(path);
        }
    }
    private void showFolderContent(){
        while(true) {
            logger.info("input path for folder");
            String path = input.nextLine();
            if(path.equals("end")){
                break;
            }
            controller.printFolder(path);
        }
    }
    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values())
            logger.info(str + "\n");
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
