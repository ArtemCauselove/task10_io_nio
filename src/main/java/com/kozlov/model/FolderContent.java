package com.kozlov.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

public class FolderContent
{
    private static Logger logger = LogManager.getLogger(FolderContent.class);
    static void RecursivePrint(File[] arr, int index, int level)
    {
        // terminate condition
        if(index == arr.length)
            return;

        // tabs for internal levels
        for (int i = 0; i < level; i++)
            logger.info("\t");

        // for files
        if(arr[index].isFile())
            logger.info(arr[index].getName());

            // for sub-directories
        else if(arr[index].isDirectory())
        {
            logger.info("[" + arr[index].getName() + "]");

            // recursion for sub-directories
            RecursivePrint(arr[index].listFiles(), 0, level + 1);
        }

        // recursion for main directory
        RecursivePrint(arr,++index, level);
    }

    // Driver Method
    public void folderContent(String name)
    {
        // Provide full path for directory(change accordingly)
        String maindirpath = name;

        // File object
        File maindir = new File(maindirpath);

        if(maindir.exists() && maindir.isDirectory())
        {
            // array for files and sub-directories
            // of directory pointed by maindir
            File arr[] = maindir.listFiles();

            logger.info("**********************************************");
            logger.info("Files from " + name + " directory : " + maindir);
            logger.info("**********************************************");

            // Calling recursive method
            RecursivePrint(arr,0,0);
        }
    }
}