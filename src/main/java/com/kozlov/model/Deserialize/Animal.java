package com.kozlov.model.Deserialize;

import java.io.Serializable;

public class Animal implements Serializable {
    private String name;
    private int age;
    private int weight;
    private String country;
    private transient String nickname;
    Animal(String name, int age, int weight, String country, String nickname){
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.country = country;
        this.nickname = nickname;
    }

    @Override
    public String toString() {
        return "Name:" + name + " age:" + age +
                "\n weight:" + weight + " coutry:" + country +
                " nickname:" + nickname;
    }
}
