package com.kozlov.model.Deserialize;

import java.io.*;

public class DeSerialisation {
    Animal animal = new Animal("Bobik", 4, 50, "Ukraine", "pushok");
    String filename = "file.txt";
public void serialize() {
    try(FileOutputStream file = new FileOutputStream(filename);
        ObjectOutputStream out = new ObjectOutputStream(file)) {
    out.writeObject(animal);
    System.out.println("Object has been serialized");
    }
    catch(IOException ex)
    {
        ex.printStackTrace();
    }
}
public void deSerialize() {
    try (FileInputStream file = new FileInputStream(filename);
        ObjectInputStream in = new ObjectInputStream(file)){
        Animal turtle = (Animal)in.readObject();
        System.out.println("Object has been deserialized ");
        System.out.println("turtle = " + turtle.toString());
    } catch (IOException | ClassNotFoundException ex) {
        System.out.println("IOException is caught");
    }
}
}
