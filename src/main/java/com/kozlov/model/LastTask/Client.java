package com.kozlov.model.LastTask;
import java.io.*;
import java.net.Socket;
public class Client extends Thread {
    public static void main(String[] args) {
        while (true) {
            try (Socket clientSocket = new Socket("localhost", 4004);
                 BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                 BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                 BufferedWriter out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()))) {
                while (true) {
                    System.out.println("input message");
                    String word = reader.readLine();
                    out.write(word + "\n");
                    out.flush();
                    String serverWord = in.readLine();
                    System.out.println(serverWord);
                }

            } catch (IOException e) {
                System.err.println(e);
            }
        }
    }
}