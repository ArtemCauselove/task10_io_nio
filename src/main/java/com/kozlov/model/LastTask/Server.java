package com.kozlov.model.LastTask;
import java.io.*;
import java.net.*;
import java.util.LinkedList;

public class Server extends Thread {
    public static final int PORT = 4004;
    public static LinkedList<Sender> serverList = new LinkedList<>();
    public static void main(String[] args) throws IOException {
        ServerSocket server = new ServerSocket(PORT);
        System.out.println("Server is working");
        try {
            while (true) {
                Socket socket = server.accept();
                System.out.println("User connected");
                try {
                    serverList.add(new Sender(socket));
                } catch (IOException e) {
                    socket.close();
                }
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}