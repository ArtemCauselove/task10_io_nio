package com.kozlov.model.LastTask;

import java.io.*;
import java.net.Socket;
class Sender extends Thread {
    private Socket socket;
    private BufferedReader in;
    private BufferedWriter out;
    public Sender(Socket socket) throws IOException {
        this.socket = socket;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        start();
    }
    @Override
    public void run() {
        String word;
        try {
            while (true) {
                word = in.readLine();
                if(word.equals("stop")) {
                    break;                }
                for (Sender vr : Server.serverList) {
                    vr.send(word);
                }
            }
        } catch (IOException e) {
        }
    }
    private void send(String msg) {
        try {
            out.write("From another user " + msg + "\n");
            out.flush();
        } catch (IOException ignored) {}
    }
}