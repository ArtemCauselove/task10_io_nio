
package com.kozlov.model.ReaderTest;

import java.io.*;
import java.net.URISyntaxException;
import java.util.ResourceBundle;

public class Writer {
    private ResourceBundle resourceBundle = ResourceBundle.getBundle("path");
    public void usualWriting() {
        try {
            File file = new File(this.getClass().getResource(resourceBundle.getString("Writer")).toURI());
            FileOutputStream out = new FileOutputStream(file);
            String name = "hellodddddddddddddddddddddddddddddddddd";
            out.write(name.getBytes());
            System.out.println("Usual writing");
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }
    public void bufferWriting(int size) {
        try {
            File file = new File(this.getClass().getResource(resourceBundle.getString("Writer")).toURI());
            FileOutputStream out = new FileOutputStream(file);
            BufferedOutputStream buffered = new BufferedOutputStream(out, size);
            String name = "hellodddddddddddddddddddddddddddddddddd";
            buffered.write(name.getBytes());
            System.out.println("buffered writing");
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }
}

