package com.kozlov.model.ReaderTest;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ResourceBundle;

public class Reader {
    private  ResourceBundle  resourceBundle = ResourceBundle.getBundle("path");
    public void usualReading() {
        try {
            File file = new File(this.getClass().getResource(resourceBundle.getString("Reader")).toURI());
            FileInputStream in = new FileInputStream(file);
            while (in.read() != -1) {
            }
            System.out.println("Usual reading");
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }
    public void bufferReading(int size) {
        try {
            File file = new File(this.getClass().getResource(resourceBundle.getString("Reader")).toURI());
            FileInputStream in = new FileInputStream(file);
            BufferedInputStream buffered = new BufferedInputStream(in, size);
            while (buffered.read() != -1) {
            }
            System.out.println("buffered reading");
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
